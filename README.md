# capture

This tool collects Unclaimed Property data from PDF files and websites.

## Installation

Install with `pip`, for example:
``pip install -e setup.py``

### Dependencies

Apart from packages listed in setup.py which will be pulled by `pip`,
this tool also requires a number of binaries unavailable for
installation through `pip`, namely:

- [tesseract](https://github.com/tesseract-ocr/tesseract);
- `pdftotext` and `pdffonts`, both are parts of the [poppler](https://poppler.freedesktop.org/) package, most modern Linux distributions come with it preinstalled.
- `exempi` library
- `qpdf`
- `gs` (ghostscript) binary

## Usage

Invoke with `capture <arguments>`. The CLI tool supports multiple branching commands:

- `web`, for downloading and parsing data from websites;
    - `keyword`, for fetching from websites based on a keyword;
    - `complete`, for fetching a range of properties.
- `pdf`, for extracting and parsing data from PDF files.

A list of possible parsers can be aquired with `-h`, for example:

- `capture web keyword -h` would show usage and available web keyword-based parsers;
- `capture pdf -h` would show usage and available pdf parsers.

### Examples

- `capture web keyword --proxy-variant tor --proxy-address socks5://127.0.0.1:9050 oregonupus out.csv`
- `capture web complete --proxy-address socks5://143.170.128.61:4000 missingmoneycom`
- `capture pdf ct_finderlist ct_finderlist1.pdf ct_finderlist2.pdf ct_finderlist3.pdf out.csv'`
