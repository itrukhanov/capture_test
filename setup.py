import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="capture",
    version="0.1.0",
    author="Auxiliary Teams",
    description="Collect unclaimed property data from the web and PDF files.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    python_requires=">=3.7",
    packages=setuptools.find_packages("src"),
    package_dir={"": "src"},
    entry_points={
        "console_scripts": ["capture=capture_cli.main:main"]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'requests[socks]',
        'stem',
        'bs4',
        'html5lib',
        'lxml',
        'ocrmypdf',
    ],
)
