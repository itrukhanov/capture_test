#!/bin/sh
yum install -y python3-pip
pip3 install https://bitbucket.org/itrukhanov/capture_test/get/master.zip
yum install -y poppler-utils exempi qpdf ghostscript
# Tesseract
yum-config-manager --add-repo https://download.opensuse.org/repositories/home:/Alexander_Pozdnyakov/CentOS_7/
rpm --import https://build.opensuse.org/projects/home:Alexander_Pozdnyakov/public_key
yum update -y
yum install -y tesseract

