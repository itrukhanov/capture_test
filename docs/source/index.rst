.. capture documentation master file, created by
   sphinx-quickstart on Sat Sep 22 17:11:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to capture's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   _modules/web.rst
   _modules/pdf.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
