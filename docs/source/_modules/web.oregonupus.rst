web.oregonupus package
======================

Submodules
----------

.. automodule:: web.oregonupus.assembler
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: web.oregonupus.extractor
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: web.oregonupus
    :members:
    :undoc-members:
    :show-inheritance:
