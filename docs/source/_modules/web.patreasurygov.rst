web.patreasurygov package
=========================

Submodules
----------

.. automodule:: web.patreasurygov.assembler
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: web.patreasurygov.extractor
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: web.patreasurygov.processor
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: web.patreasurygov
    :members:
    :undoc-members:
    :show-inheritance:
