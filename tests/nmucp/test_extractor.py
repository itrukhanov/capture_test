from pdf.nmucp.extractor import parse_entry


def test_parse_entry():
    offsets = (
        0,
        13,
        39,
        78,
        100,
        111,
        156,
        175,
    )
    in_data = (
        ("9903023022   CORPUS EVELYN &           7808 HOMING PIGEON ST NORTH LAS        BANKERS LIFE &        4/28/14    IN03 - DEATH BENEFITS-BENEFICIARIES          $11,328.50         0N\n", "             BEVERLY                   VEGAS NV 89084                         CASUALTY COMPANY\n"),
        ("2003028284   DWORAK REV TRST TOM       4854 RAMCREEK TRL RENO NV              US BANK               11/1/02    AC01 - CHECKING ACCOUNTS                      $7,068.56         0N\n", "                                       89509-8029\n"),
        ("9903354394   REYNOLDS STEPHEN          4801 SPENCER ST 103 LAS VEGAS NV       GENWORTH LIFE &       1/30/15    IN03 - DEATH BENEFITS-BENEFICIARIES           $5,979.04         0N\n", "             ESTATE                    89119                                  ANNUITY INSURANCE\n", "                                                                              COMPANY\n"),
    )
    out_data = (
        ('9903023022', 'CORPUS EVELYN & BEVERLY', '7808 HOMING PIGEON ST NORTH LAS VEGAS NV 89084', 'BANKERS LIFE & CASUALTY COMPANY', '4/28/14', 'IN03 - DEATH BENEFITS-BENEFICIARIES', '$11,328.50', '0N'),
        ('2003028284', 'DWORAK REV TRST TOM', '4854 RAMCREEK TRL RENO NV 89509-8029', 'US BANK', '11/1/02', 'AC01 - CHECKING ACCOUNTS', '$7,068.56', '0N'),
        ('9903354394', 'REYNOLDS STEPHEN ESTATE', '4801 SPENCER ST 103 LAS VEGAS NV 89119', 'GENWORTH LIFE & ANNUITY INSURANCE COMPANY', '1/30/15', 'IN03 - DEATH BENEFITS-BENEFICIARIES', '$5,979.04', '0N'),
    )

    for lines, entry in zip(in_data, out_data):
        parsed_entry = parse_entry(lines, offsets)
        formatted_entry = (' '.join(col.split()) for col in parsed_entry)
        assert tuple(formatted_entry) == entry
