import itertools

from capture.pdf.base import Assembler
from .extractor import parse_buffer

class CtfinderlistAssembler(Assembler):
    @staticmethod
    def get_name():
        return 'ct_finderlist'

    @staticmethod
    def build_csv(buf):
        columns = (
            'Owner',
            'Street1',
            'City',
            'State',
            'Zip',
            'HolderName',
            'ReportYear',
            'PropertyType',
            'Description',
            'PropertyId',
            'Cash',
            'Shares',
        )

        return itertools.chain((columns,), parse_buffer(buf))
