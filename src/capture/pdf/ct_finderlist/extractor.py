import itertools
import re


def parse_buffer(buf):
    pages = pages_gen(buf)
    props = list(parse_page(next(pages)))
    # The way we parse produces an empty entry at the beginning.
    props.pop(0)
    last = props.pop()
    for p in props:
        yield format_property(p)

    for page in pages:
        # The last entry on a page overflows to the next page, so we save
        # it and manually join with the first entry of the next page.
        props = list(parse_page(page))
        props[0] = last + props[0]
        last = props.pop()
        for p in props:
            yield format_property(p)

    exclude_line_triggers = (
        'Total',
        'Print',
        'Property',
    )
    last = [line for line in last
            if not any(col for col in line
                       if any(t in col for t in exclude_line_triggers))]

    yield format_property(last)


def parse_page(page):
    headers = (
        'OWNER NAME',
        'ADDRESS',
        # So we don't match "OWNER NAME".
        '  NAME',
        'YEAR',
        'TYPE',
        'DESCRIPTION',
        'ID',
        'CASH',
        'SHARES',
    )

    offset_line = next(line for line in page if line.startswith(headers[0]))
    offsets = [offset_line.index(h) for h in headers]
    for entry in entries_gen(page, offsets[-1]):
        yield parse_entry(entry, offsets)


def pages_gen(buf):
    page_separator = 'Page # '

    lines = []
    for line in buf:
        if page_separator in line:
            yield iter(lines)
            lines.clear()
        lines.append(line)


def entries_gen(page, offset_last):
    lines = []
    for line in page:
        if line[offset_last:]:
            yield lines
            lines.clear()
        lines.append(line)

    yield lines


def parse_entry(entry, offsets):
    lines_split = [[line[start:end]
                    for start, end
                    in itertools.zip_longest(offsets, offsets[1:])]
                   for line in entry]

    # Quirk for address overflow:
    # "PRENDEGAST                    "|"  AETNA LIFE &"
    # "                              "|"  CASUALTY CO"
    # "LINDA                         "|""
    # "BURGER & BURGER INC BUFFR 4021"|" MAIN"
    # (pipes indicate column edges as perceived by parser)
    for i in range(1, len(lines_split)):
        if not lines_split[i - 1][2] and lines_split[i][2]:
            lines_split[i][1] += lines_split[i][2]
            lines_split[i][2] = ''

    return lines_split


def format_property(prop):
    owner_name = ''
    address = []
    rest = [''] * len(prop[0])

    for line in prop:
        owner_name += line[0]
        address.append(line[1])
        rest = [old_col + new_col for old_col, new_col in zip(rest, line[2:])]

    columns = [owner_name] + parse_address(address) + rest

    return [' '.join(c.split()) for c in columns]


def parse_address(lines):
    state_lines = [None, None]
    for i, line in enumerate(lines):
        # Definitely a line with state.
        # Quirk for:
        # "CAMBRIDGE      CT"
        # "ONT N"
        if re.search(r'\s\s\S\S?\s', line):
            state_lines[0] = i
        elif re.search(r'\s\S\S?\s', line):
            state_lines[1] = i

    state_line = state_lines[0] if state_lines[0] else state_lines[1]

    address = ' '.join(lines[:state_line])
    city = ''
    state = ''
    zp = ''
    if state_line:
        line_split = lines[state_line].rsplit(maxsplit=2)
        last_has_numeric = any(c.isnumeric() for c in line_split[-1])
        last_is_zip = last_has_numeric and len(line_split[-1]) > 2
        if last_is_zip:
            if len(line_split) > 2:
                city = line_split[-3]

            state = line_split[-2]
            zp = line_split[-1]

        else:
            if len(line_split) > 1:
                city = ' '.join(line_split[:-1])

            state = line_split[-1]

        city += ' ' + ' '.join(lines[state_line + 1:])

    return [address, city, state, zp]
