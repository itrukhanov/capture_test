from .nmucp import NmucpAssembler
from .ct_finderlist import CtfinderlistAssembler
from .wayne_county_mi import WaynecountymiAssembler

#: Children of :class:`~pdf.base.Assembler` must be listed here
#: in order to be available to the user.
assemblers = (
    CtfinderlistAssembler,
    NmucpAssembler,
    WaynecountymiAssembler
)
_names = (assembler.get_name() for assembler in assemblers)
assemblers = {k: v for k, v in zip(_names, assemblers)}
