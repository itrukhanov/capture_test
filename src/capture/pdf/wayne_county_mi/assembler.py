from capture.pdf.base import Assembler
from .extractor import parse
from .processor import process


class WaynecountymiAssembler(Assembler):
    @staticmethod
    def get_name():
        return 'wayne_county_mi'

    @staticmethod
    def build_csv(buf):
        return process(parse(buf))
