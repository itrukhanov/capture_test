import itertools


def parse(buf):
    return entries_gen(words_gen(buf))


def words_gen(buf):
    for line in buf:
        for word in line.split('  '):
            if word:
                yield word.strip()


def entries_gen(words):
    word = ''
    for word in words:
        if is_id(word):
            break

    words = itertools.chain((word,), words)

    entry = []
    for word in words:
        if is_id(word):
            yield entry
            entry.clear()
            entry.append(word)

        if len(entry) == 3:
            entry.append(word)

        if len(entry) == 2 and ('-' in word or '—' in word):
            entry.append(word)

        if len(entry) == 1 and ('.' in word or ',' in word):
            entry.append(word)


def is_id(word):
    return word.replace(' ', '').isdigit()
