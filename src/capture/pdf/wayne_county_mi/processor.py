def process(entries):
    yield (
        'SERIAL NUMBER',
        'AMOUNT',
        'DATE ISSUED',
        'PAYEE IDENTIFICATION',
    )

    entries = list(map(list, entries))
    entries = filter(is_correct, entries)

    for entry in entries:
        entry[0] = entry[0].replace(' ', '')
        entry[1] = entry[1].strip('.,').replace(' ', '')
        entry[2] = entry[2].replace(' ', '')

        yield entry


def is_correct(entry):
    if len(entry) != 4:
        return False

    ignored_substrings = (
        'Check Paid This Period, No Issue Received',
        'TEAM-NO',
        'TREASURER',
        'WAYNE CO.',
        'AS OF',
        'Prior Period Paid No Issue',
    )
    for substr in ignored_substrings:
        if any(col for col in entry if substr in col):
            return False

    if all(not c.isalpha() for c in entry[3]):
        return False

    if all(not c.isdigit() for c in entry[1]):
        return False

    if '-' not in entry[2] and '—' not in entry[2]:
        return False

    return True
