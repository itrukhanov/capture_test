import subprocess


def check_pdf_for_text(pdf_path):
    output = subprocess.check_output(('pdffonts', pdf_path))

    # The first two lines are decorative, more means text is present.
    return len(output.splitlines()) > 2


def ocr_pdf(pdf_path):
    """
    Run `ocrmypdf` on specified file.

    Modifies the file in place if OCR is successful.
    """

    ocrmypdf_arguments = (
        'ocrmypdf',
        '--output-type',
        'pdf',
        pdf_path,
        pdf_path,
    )

    subprocess.run(ocrmypdf_arguments)


def pdf_to_text(pdf_file):
    pdftotext_arguments = (
        'pdftotext',
        '-layout',
        pdf_file,
        '-',
    )

    process = subprocess.run(pdftotext_arguments, capture_output=True)

    return process.stdout.decode('utf-8').splitlines()
