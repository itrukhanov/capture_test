from itertools import zip_longest, islice


def parse(buf):
    for page in pages_gen(buf):
        # First "entry" is empty.
        for entry in islice(parse_page(page), 1, None):
            yield entry


def pages_gen(buf):
    page = []
    for line in buf:
        if 'Page' in line:
            yield iter(page)
            page.clear()
            continue

        page.append(line)


def parse_page(page):
    offset_strings = (
        'Owner Name',
        'Reported Address',
        'Holder Name',
        'Recv Dt',
        'Property Type',
        'ACCNT_NO',
        'Cash Value',
        'Shrs',
    )

    for line in page:
        if 'Recv Dt' in line:
            break

    offsets = [0] + [line.find(off_str) - 1 for off_str in offset_strings]

    return (parse_entry(entry, offsets) for entry in entries_gen(page))


def entries_gen(page):
    entry = []
    for line in page:
        if not line[0].isspace():
            yield entry
            entry.clear()

        entry.append(line)

    yield entry


def parse_entry(entry, offsets):
    columns = ([line[start:end] for start, end
                in zip_longest(offsets, offsets[1:], fillvalue=None)]
               for line in entry)
    columns = map(list, zip_longest(*columns))
    columns = (''.join(row) for row in columns)

    return columns
