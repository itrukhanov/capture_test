import requests

from itertools import chain, islice, repeat
from operator import itemgetter


def process(entries):
    f = requests.get('https://cdn.rawgit.com/grammakov/USA-cities-and-states/20fa8f6a/us_cities_states_counties.csv').text
    cities = set(line.split('|')[0].upper() for line in f.splitlines())

    for entry in entries:
        entry = (column.strip() for column in entry)
        entry = [' '.join(column.split()) for column in entry]
        entry[2:3] = _disassemble_address(entry[2], cities)
        # Property Type column overflows into ACCNT_NO.
        entry[8:10] = [''.join(entry[8:10])]
        # Shrs and SK end up joined.
        shrs_and_sk = entry[-1]
        entry[-1:] = [shrs_and_sk[:-1], shrs_and_sk[-1:]]

        yield entry


def _disassemble_address(line, cities):
    matches = ((city, line.index(city)) for city in cities if city in line)
    matches = sorted(matches, key=itemgetter(1), reverse=True)
    matches = (match for match, _ in matches)
    city = ''
    for match in islice(matches, None, 1):
        city = match

    for match in matches:
        if city in match:
            city = match

    addr, city, rest = line.rpartition(city) if city else (line, city, '')
    rest = rest.rsplit(maxsplit=1)
    rest = chain(rest, repeat(''))
    state, zp = islice(rest, None, 2)
    if any(c.isnumeric() for c in state):
        state, zp = zp, state

    # End up with a leading space after rpartition.
    addr = addr.strip()
    state = state.strip()

    return (addr, city, state, zp)
