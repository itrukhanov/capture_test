import itertools

from capture.pdf.base import Assembler
from . import extractor, processor


class NmucpAssembler(Assembler):
    @staticmethod
    def get_name():
        return 'nmucp'

    @staticmethod
    def build_csv(buf):
        headers = (
            'PID',
            'Owner Name',
            'Reported Address',
            'City',
            'State',
            'Zip',
            'Holder Name',
            'Recv Dt',
            'Property Type',
            'Cash Value',
            'Shrs',
            'SK',
        )

        entries = extractor.parse(buf)
        processed_entries = processor.process(entries)

        return itertools.chain((headers,), processed_entries)
