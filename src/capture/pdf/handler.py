import csv
import itertools

from .processing import check_pdf_for_text
from .processing import ocr_pdf
from .processing import pdf_to_text


def handle(files, assembler, output_path):
    for f in files:
        if not check_pdf_for_text(f):
            ocr_pdf(f)

    lines = itertools.chain(*(pdf_to_text(f) for f in files))
    with open(output_path, 'w') as csvfile:
        writer = csv.writer(csvfile, quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerows(assembler.build_csv(lines))
