import abc


class Assembler(abc.ABC):
    """Every parser must inherit this class."""
    @abc.abstractstaticmethod
    def get_name():
        raise NotImplementedError()

    @abc.abstractstaticmethod
    def build_csv(buf):
        raise NotImplementedError
