import itertools

import requests
from bs4 import BeautifulSoup


def fetch(keyword, proxies):
    data = {
        'Submit': 'Search Properties',
        'LastName': keyword,
    }
    url = 'https://oregonup.us/upweb/up/UP_search.asp'

    return requests.post(url, data, proxies=proxies, timeout=15).text


def parse(html):
    bs = BeautifulSoup(html, 'html5lib')
    table = bs.find('form', {'name': 'PropList'}).find('table')
    trs = iter(table.find_all('tr'))

    headers = (th.get_text().strip() for th in next(trs).find_all('th'))
    headers = (th for th in headers if th)
    headers = itertools.chain(headers, ('Description', 'Reported Year'))

    yield headers

    values, info = itertools.tee(trs)
    values = itertools.islice(values, None, None, 3)
    info = itertools.islice(info, 1, None, 3)

    values = (tr.find_all('td') for tr in values)
    info = (tr.find_all('td') for tr in info)

    values = (itertools.islice(tr, 1, None) for tr in values)
    info = (itertools.islice(tr, 1, None) for tr in info)

    values = (map(lambda x: x.get_text().strip(), tr) for tr in values)
    info = (map(lambda x: x.get_text().split(':')[1].strip(), tr) for tr in info)

    for values_row, info_row in zip(values, info):
        yield itertools.chain(values_row, info_row)
