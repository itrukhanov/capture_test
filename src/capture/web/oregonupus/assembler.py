from capture.web.base import KeywordAssembler
from . import extractor


class OregonupusAssembler(KeywordAssembler):
    @staticmethod
    def get_name():
        return 'oregonupus'

    @staticmethod
    def build_csv(keyword, proxies):
        html = extractor.fetch(keyword, proxies)
        entries = extractor.parse(html)

        return entries
