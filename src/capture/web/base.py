import abc
import typing

from capture.web.Proxy import Proxy


class Assembler(abc.ABC):
    def __init__(self, proxy: Proxy):
        self.proxy = proxy

    @abc.abstractstaticmethod
    def get_name():
        raise NotImplementedError()

    @abc.abstractmethod
    def build_csv() -> typing.Generator[list, None, None]:
        raise NotImplementedError


class KeywordAssembler(Assembler):
    """Every keyword-based parser must inherit this class."""
    pass


class CompleteAssembler(Assembler):
    """Every complete parser must inherit this class."""
    pass
