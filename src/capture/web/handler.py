import csv
import time

import requests
from stem import Signal
from stem.control import Controller

from capture.web.Proxy import Proxy, Variant
from . import keyword_assemblers
from . import complete_assemblers


def handle(args):
    proxy = Proxy(Variant(args.proxy_variant), args.proxy_address)

    proxies = {
        'http': proxy.address,
        'https': proxy.address,
    }

    if args.type == 'keyword':
        _handle_keyword(args.website, args.name, proxy, proxies, args.output)

    elif args.type == 'complete':
        _handle_complete(args.website, args.count, proxy, proxies, args.output)


def _handle_keyword(website, keyword, proxy, proxies, output):
    if proxy.variant == Variant.TOR:
        _request_new_tor_identity(proxies)

    csv_list = keyword_assemblers[website].build_csv(keyword, proxies)
    with open(output, 'w') as f:
        writer = csv.writer(f)
        for row in csv_list:
            writer.writerow(row)


def _handle_complete(website, count, proxy, proxies, output):
    assembler = complete_assemblers[website]
    csv_list = assembler.build_csv(proxies, count)
    with open(output, 'w') as f:
        writer = csv.writer(f)
        for row in csv_list:
            writer.writerow(row)
            # TODO: This _really_ needs to be a callback or something.
            if proxy.variant == Variant.TOR:
                _request_new_tor_identity(proxies)


def _request_new_tor_identity(proxies):
    old_ip = new_ip = requests.get('http://icanhazip.com/', proxies=proxies, timeout=15).text
    with Controller.from_port(port=9051) as controller:
        controller.authenticate(password='test')
        while not controller.is_newnym_available():
            time.sleep(1)

        controller.signal(Signal.NEWNYM)
        controller.close()

    while old_ip == new_ip:
        time.sleep(1)
        new_ip = requests.get('http://icanhazip.com/', proxies=proxies, timeout=15).text
        print(new_ip)
