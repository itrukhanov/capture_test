import requests
from bs4 import BeautifulSoup


def fetch(proxies, ngo):
    url = 'http://pacer.flmb.uscourts.gov/fwxflmb/fud/fud.fwx'
    data = {
        'morder': 'CNU',
        'ngo': ngo,
    }

    return requests.post(url, data=data, timeout=15).text


def parse(html):
    bs = BeautifulSoup(html, 'lxml')
    trs = bs.find_all('tr')
    entries = (tr.find_all('td') for tr in trs)
    ngo = bs.find('input', {'name': 'ngo'})['value']

    return (entries, ngo)
