from capture.web.base import CompleteAssembler
from .extractor import fetch, parse
from .processor import process

class FudfloridaAssembler(CompleteAssembler):
    @staticmethod
    def get_name():
        return 'fud_florida'

    @staticmethod
    def build_csv(proxies, entries_count):
        entries_extracted = 0
        ngo = '1'

        while entries_extracted < entries_count or entries_count < 0:
            entries, ngo = parse(fetch(proxies, ngo))
            for entry in process(entries):
                yield entry
                entries_extracted += 1
