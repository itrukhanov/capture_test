def process(entries):
    for entry in entries:
        entry = (td.get_text() for td in entry)
        entry = ('\n'.join((line.strip() for line in td.split('\n') if line))
                 for td in entry)
        yield (td for td in entry if td)
