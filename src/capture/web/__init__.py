from .marylandtaxescom import MarylandtaxescomAssembler
from .oregonupus import OregonupusAssembler
from .patreasurygov import PatreasurygovAssembler
from .treasurerlynnfitchcom import TreasurerlynnfitchcomAssembler
from .missingmoneycom import MissingmoneycomAssembler
from .fud_florida import FudfloridaAssembler
from .cd_california import CdcaliforniaAssembler

#: Children of :class:`~web.base.KeywordAssembler` must be listed here
#: in order to be available to the user.
keyword_assemblers = (
    MarylandtaxescomAssembler,
    OregonupusAssembler,
    PatreasurygovAssembler,
    TreasurerlynnfitchcomAssembler,
)
_names = (assembler.get_name() for assembler in keyword_assemblers)
keyword_assemblers = {k: v for k, v in zip(_names, keyword_assemblers)}

#: Children of :class:`~web.base.CompleteAssembler` must be listed here
#: in order to be available to the user.
complete_assemblers = (
    MissingmoneycomAssembler,
    FudfloridaAssembler,
    CdcaliforniaAssembler,
)
_names = (assembler.get_name() for assembler in complete_assemblers)
complete_assemblers = {k: v for k, v in zip(_names, complete_assemblers)}
