from capture.web.base import CompleteAssembler
from .extractor import fetch


class CdcaliforniaAssembler(CompleteAssembler):
    @staticmethod
    def get_name():
        return 'cd_california'

    @staticmethod
    def build_csv(proxies, min_entries_to_fetch):
        entries = fetch(proxies)
        first_entry, *entries = entries
        yield first_entry.keys()
        yield first_entry.values()
        for entry in entries:
            yield entry.values()
