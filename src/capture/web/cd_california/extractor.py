import requests


def fetch(proxies):
    url = 'http://ecf-ciao.cacb.uscourts.gov/UnclaimedFunds/GetUF/'
    data = {
        'division': 'All',
    }

    return requests.get(url, data=data, proxies=proxies, timeout=15).json()
