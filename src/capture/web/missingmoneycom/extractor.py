import requests
from bs4 import BeautifulSoup


class PropertyNotFoundError(Exception):
    pass


def fetch(proxies, up_id, up_state_id):
    url = 'https://www.missingmoney.com/Main/ClaimEligibility.cfm'
    data = {
        'CheckClaimId': '',
        'CheckName': '',
        'FormMultiClaim': 'N',
        'MMId': 'DYSI1001ZZ000000000000000000',
        'NewSearch': 'Y',
        'RowStart': '1',
        'SearchCity': '',
        'SearchExactMatch': 'N',
        'SearchFirstName': '',
        'SearchId': '',
        'SearchLastName': '',
        'SearchMiddleName': '',
        'SearchStateId': '',
        'UPId': up_id,
        'UPStateId': up_state_id,
    }

    return requests.post(url, data=data, proxies=proxies, timeout=15).text


def parse(html):
    bs = BeautifulSoup(html, 'html5lib')
    table = bs.find_all('table')[2]
    if len(table.find_all('tr')) < 2:
        raise PropertyNotFoundError('Specified property ID not found.')
    row = table.find_all('tr')[1]
    cols = row.find_all('td')

    return (col.text.strip() for col in cols)
