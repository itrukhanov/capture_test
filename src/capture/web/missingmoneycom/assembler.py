import itertools

from capture.web.base import CompleteAssembler
from . import extractor


class MissingmoneycomAssembler(CompleteAssembler):
    @staticmethod
    def get_name():
        return 'missingmoneycom'

    @staticmethod
    def build_csv(proxy, entries_count):
        headers = (
            'State Property Id',
            'Name',
            'Last Known Address',
            'Property Type',
            'Reported By',
            'Amount',
        )
        yield headers

        states = (
            'AK',
        )

        entries_processed = 0
        property_id = itertools.count(1)
        while entries_processed < entries_count or entries_count < 0:
            html = extractor.fetch(proxy.to_proxies(), next(property_id), states[0])
            try:
                yield extractor.parse(html)
            except extractor.PropertyNotFoundError:
                print('error')
                continue
            entries_processed += 1
