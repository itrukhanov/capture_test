import itertools
import requests


def fetch(keyword, proxies):
    url = 'https://www.msegov.com/mtd/treasury_admin/Json/GetUnclaimedProperty/'

    return requests.get(url + keyword, proxies=proxies, timeout=15).json()


def parse(json):
    head, *tail = json['Items']
    headers = head.keys()
    tail = (elem.values() for elem in tail)

    return itertools.chain((headers, head.values()), tail)
