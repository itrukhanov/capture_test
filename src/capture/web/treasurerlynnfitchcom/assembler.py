from capture.web.base import KeywordAssembler
from . import extractor


class TreasurerlynnfitchcomAssembler(KeywordAssembler):
    @staticmethod
    def get_name():
        return 'treasurerlynnfitchcom'

    @staticmethod
    def build_csv(keyword, proxies):
        json = extractor.fetch(keyword, proxies)
        entries = extractor.parse(json)

        return entries
