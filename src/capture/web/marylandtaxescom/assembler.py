from . import extractor, processor
from capture.web.base import KeywordAssembler


class MarylandtaxescomAssembler(KeywordAssembler):
    @staticmethod
    def get_name():
        return 'marylandtaxescom'

    @staticmethod
    def build_csv(keyword, proxies):
        html = extractor.fetch(keyword, proxies)
        entries = extractor.parse(html)

        return processor.format(entries)
