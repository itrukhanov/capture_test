import requests
from bs4 import BeautifulSoup


def fetch(keyword, proxies):
    url = 'https://interactive.marylandtaxes.com/Individuals/Unclaim/default.aspx'
    cookies = {
        'ASP.NET_SessionId': 'zd4y5s45p0ytnin5jod4r4fy',
    }
    post_data = {
        '__VIEWSTATE': '/wEPDwUKLTQ2NDM4NzM3OQ9kFgICCA9kFgpmDw9kFgYeBHNpemUFAjIwHgltYXhsZW5ndGgFAjE1HgdvbmZvY3VzBRVyZXR1cm4gdGhpcy5zZWxlY3QoKTtkAgEPD2QWBh8ABQIxNB8BBQIxNB8CBRVyZXR1cm4gdGhpcy5zZWxlY3QoKTtkAgIPD2QWAh4Hb25jbGljawUqcmV0dXJuIENoZWNrRm9ybShkb2N1bWVudC5mb3Jtc1snZm9ybTEnXSk7ZAIDDw8WAh4HVmlzaWJsZWhkZAIGDw8WAh4EVGV4dAUOMDkvMDQvMjAxOCAgICBkZGQDOwf8lx0und/ASWdd6mvZMwI+0A==',
        '__EVENTVALIDATION': '/wEWBAL5yOmFAQK7zu7HAgK7zqYkAqWf8+4K7tkG3aZJyaM3WWulHBc+eke3blM=',
        'txtLName': keyword,
        'btnSearch': 'Search',
    }
    headers = {
        'Host': 'interactive.marylandtaxes.gov',
    }
    html = requests.post(url, cookies=cookies, data=post_data, headers=headers, proxies=proxies, timeout=15).text

    return html


def parse(html):
    bs = BeautifulSoup(html, 'html5lib')
    uptable = bs.find(id='dgUnclaimedPR')
    trs = uptable.find_all('tr')
    # Last table row is not an unclaimed property entry.
    data = ((td.get_text() for td in tr.find_all('td'))
            for tr in trs[:-1])

    return data
