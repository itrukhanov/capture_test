import enum


class Variant(enum.Enum):
    PLAIN = 'plain'
    TOR = 'tor'


class Proxy:
    def __init__(self, variant: Variant, address: str):
        self.variant = variant
        self.address = address

    def to_proxies(self):
        return {
            'http': self.address,
            'https': self.address,
        }
