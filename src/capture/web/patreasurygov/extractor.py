import requests
from bs4 import BeautifulSoup


def fetch(keyword, proxies):
    url = 'https://www.patreasury.gov/Unclaimed/SearchResults.asp'
    data = {
        'LastName_VC': keyword,
    }

    return requests.post(url, data=data, proxies=proxies, timeout=15).text


def parse(html):
    bs = BeautifulSoup(html, 'lxml')
    table = bs.find('table')
    table_rows = table.find_all('tr')
    table_data = (tr.find_all('td') for tr in table_rows)
    # First column is an input, useless for us.
    table_data = ((td.get_text() for td in row[1:])
                  for row in table_data)

    return table_data
