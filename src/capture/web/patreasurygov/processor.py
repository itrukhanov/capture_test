def format(entries):
    return ((col.strip() for col in entry) for entry in entries)
