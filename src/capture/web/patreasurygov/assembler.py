from capture.web.base import KeywordAssembler
from . import extractor, processor


class PatreasurygovAssembler(KeywordAssembler):
    @staticmethod
    def get_name():
        return 'patreasurygov'

    @staticmethod
    def build_csv(keyword, proxies):
        html = extractor.fetch(keyword, proxies)
        entries = extractor.parse(html)

        return processor.format(entries)
