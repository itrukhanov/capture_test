import argparse
import enum

import capture.web.handler
import capture.pdf.handler
from capture.web.Proxy import Variant


class Parsers(enum.Enum):
    WEB = 'web'
    PDF = 'pdf'


def main():
    plain = Variant.PLAIN.value
    proxy_variant_arguments = {
        'default': plain,
        'choices': [e.value for e in Variant],
        'help': f'default is "{plain}", ignored if proxy-address is absent',
    }

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='command')
    subparsers.required = True
    parser_web = subparsers.add_parser(Parsers.WEB.value)
    parser_pdf = subparsers.add_parser(Parsers.PDF.value)

    subparsers = parser_web.add_subparsers(dest='type')
    subparsers.required = True
    parser_web_keyword = subparsers.add_parser('keyword')
    parser_web_complete = subparsers.add_parser('complete')

    parser_web_keyword.add_argument('website', choices=capture.web.keyword_assemblers)
    parser_web_keyword.add_argument('name')
    parser_web_keyword.add_argument('--proxy-variant', **proxy_variant_arguments)
    parser_web_keyword.add_argument('--proxy-address')
    parser_web_keyword.add_argument('output', default='out.csv')
    parser_web_complete.add_argument('website', choices=capture.web.complete_assemblers)
    parser_web_complete.add_argument('--count', default=-1, type=int, help='extract at least this number of entries; negative values will run indefinitely; default is "-1"')
    parser_web_complete.add_argument('--proxy-variant', **proxy_variant_arguments)
    parser_web_complete.add_argument('--proxy-address')
    parser_web_complete.add_argument('output', default='out.csv')

    parser_pdf.add_argument('name', choices=capture.pdf.assemblers)
    parser_pdf.add_argument('files', nargs='+')
    parser_pdf.add_argument('output')

    args = parser.parse_args()

    if args.command == Parsers.WEB.value:
        capture.web.handler.handle(args)
    elif args.command == Parsers.PDF.value:
        assembler = capture.pdf.assemblers[args.name]
        capture.pdf.handler.handle(args.files, assembler, args.output)
